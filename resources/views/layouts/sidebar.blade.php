<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="#">Stisla</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="#">St</a>
        </div>
        <ul class="sidebar-menu">

            <li class="menu-header">Dashboard</li>
            </li>
            <li>
                <a href="#" class="nav-link">
                    <i class="fas fa-users"></i>
                    <span>User</span>
                </a>
            </li>

            
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown">
                    <i class="fas fa-cogs"></i>
                    <span>Manajemen RBAC</span>
                </a>
                <ul class="dropdown-menu" >
                    <li>
                        <a href="#" class="nav-link">
                            <i class="fas fa-users"></i>
                            <span>User</span>
                        </a>
                    </li>
                    <li >
                        <a href="#" class="nav-link">
                            <i class="fas fa-user-tag"></i>
                            <span>Role</span>
                        </a>
                    </li>
                </ul>
            </li>


        </ul>
    </aside>
</div>
